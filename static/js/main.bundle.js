var main =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/js/index.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/components/input-tel.js":
/*!****************************************!*\
  !*** ./src/js/components/input-tel.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {$('input[type=\"tel\"]').mask(\"+7 (999) 999-99-99\");\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/input-tel.js?");

/***/ }),

/***/ "./src/js/components/map.js":
/*!**********************************!*\
  !*** ./src/js/components/map.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {var myMap;\nvar maps = [];\n/**\n * Иницилизация карты\n * Заранее требуется глобальная переменная myMap\n */\n\nfunction initYmap() {\n  $(\".map-container\").each(function (i, e) {\n    if (!$(e).length) return false;\n    maps[i] = {\n      map_id: $(e).attr('id'),\n      markers: []\n    };\n    $(e).find('div').each(function (div_i, el) {\n      maps[i].markers.push({\n        lat: $(el).attr('data-lat').replace(/,/, '.'),\n        lng: $(el).attr('data-lng').replace(/,/, '.'),\n        popup: $(el).attr('data-popup'),\n        message: $(el).attr('data-message')\n      });\n      $(el).remove();\n    }); // Иницилизируем карту\n\n    myMap = new ymaps.Map(maps[i].map_id, {\n      center: [maps[i].markers[0].lat, maps[i].markers[0].lng],\n      zoom: 14,\n      controls: []\n    }, {\n      searchControlProvider: 'yandex#search'\n    }); // Создаём макет содержимого.\n\n    var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div style=\"color: #FFFFFF; font-weight: bold;\">$[properties.iconContent]</div>'); // Добавляем на карту маркеры\n\n    var _iteratorNormalCompletion = true;\n    var _didIteratorError = false;\n    var _iteratorError = undefined;\n\n    try {\n      for (var _iterator = maps[i].markers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {\n        var marker = _step.value;\n        var placemark = new ymaps.Placemark([marker.lat, marker.lng], {\n          hintContent: marker.message,\n          balloonContent: marker.popup\n        });\n        myMap.geoObjects.add(placemark);\n      }\n    } catch (err) {\n      _didIteratorError = true;\n      _iteratorError = err;\n    } finally {\n      try {\n        if (!_iteratorNormalCompletion && _iterator.return != null) {\n          _iterator.return();\n        }\n      } finally {\n        if (_didIteratorError) {\n          throw _iteratorError;\n        }\n      }\n    }\n\n    myMap.events.add('sizechange', function () {\n      myMap.setBounds(myMap.geoObjects.getBounds(), {\n        checkZoomRange: true\n      });\n    });\n  });\n}\n\nif (window.ymaps !== undefined) {\n  ymaps.ready(function () {\n    initYmap();\n  });\n}\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/map.js?");

/***/ }),

/***/ "./src/js/components/text-page.js":
/*!****************************************!*\
  !*** ./src/js/components/text-page.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {function resizeContent() {\n  $('.content img').each(function (i, e) {\n    var w_post_img = $(e).width();\n    var h_post_img = w_post_img * 32 / 64;\n    $(e).css('height', h_post_img);\n  });\n  $('.gallery-item-thumbnail, .certificate-thumbnail').each(function (i, e) {\n    var w_gallery_img = $(e).width();\n    var h_gallery_img = w_gallery_img / 1.5;\n    $(e).css('height', h_gallery_img);\n  });\n}\n\nresizeContent();\n$(window).resize(function () {\n  resizeContent();\n});\n/*     Обертка таблицы на текстовых    */\n\n$('.content-text > table').prev('h3').addClass('table-title');\n$('.content-text > table').wrap('<div class=\"table\"><div class=\"table-responsive\"></div></div>');\n$('.content-text > .table').each(function () {\n  $(this).prev('h3.table-title').prependTo($(this));\n});\n$('img.lazy').each(function (index, el) {\n  $(el).parent().addClass('lazy_wrap');\n  $(el).lazy({\n    afterLoad: function afterLoad(element) {\n      $(el).parent().removeClass('lazy_wrap');\n    }\n  });\n});\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/text-page.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _vendors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vendors */ \"./src/js/vendors.js\");\n/* harmony import */ var _components_text_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/text-page */ \"./src/js/components/text-page.js\");\n/* harmony import */ var _components_text_page__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_text_page__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _components_input_tel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/input-tel */ \"./src/js/components/input-tel.js\");\n/* harmony import */ var _components_input_tel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_input_tel__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _components_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/map */ \"./src/js/components/map.js\");\n/* harmony import */ var _components_map__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_map__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n // Import new components here\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/index.js?");

/***/ }),

/***/ "./src/js/vendors.js":
/*!***************************!*\
  !*** ./src/js/vendors.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap */ \"./node_modules/bootstrap/dist/js/bootstrap.js\");\n/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var dotdotdot__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotdotdot */ \"./node_modules/dotdotdot/src/js/jquery.dotdotdot.js\");\n/* harmony import */ var dotdotdot__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotdotdot__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var jquery_lazy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery-lazy */ \"./node_modules/jquery-lazy/jquery.lazy.js\");\n/* harmony import */ var jquery_lazy__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery_lazy__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! slick-carousel */ \"./node_modules/slick-carousel/slick/slick.js\");\n/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fancyapps/fancybox/dist/jquery.fancybox.min.js */ \"./node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js\");\n/* harmony import */ var _fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var svg_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! svg.js */ \"./node_modules/svg.js/dist/svg.js\");\n/* harmony import */ var svg_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(svg_js__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _vendors_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./vendors/mask */ \"./src/js/vendors/mask.js\");\n/* harmony import */ var _vendors_mask__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_vendors_mask__WEBPACK_IMPORTED_MODULE_7__);\n\n\n\n\n\n\n\n\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/vendors.js?");

/***/ })

/******/ });